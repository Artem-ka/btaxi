<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="i18n.msg"/>
<%--
  Created by IntelliJ IDEA.
  User: Roman
  Date: 27.12.2016
  Time: 17:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <form action="">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registration</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-0 px-0">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item ml-3">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                               aria-controls="home" aria-selected="true">User form</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                               aria-controls="profile" aria-selected="false">Driver form</a>
                        </li>
                    </ul>
                    <div class="container my-2">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon">
                                        <i class="fa fa-fw fa-user my-1"></i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" placeholder="First name" aria-describedby="basic-addon" aria-label="firstname">
                                <input type="text" class="form-control" placeholder="Second name" aria-describedby="basic-addon" aria-label="secondname">
                                <input type="text" class="form-control" placeholder="Last name" aria-describedby="basic-addon" aria-label="lastname">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon4">
                                    <i class="fa fa-fw fa-envelope-o my-1"></i>
                                </span>
                                </div>
                                <input type="text" class="form-control" placeholder="E-mail address" aria-label="Username"
                                       aria-describedby="basic-addon4">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon5"><i class="fa fa-fw fa-mobile my-1"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Phone number" aria-label="Username"
                                       aria-describedby="basic-addon4">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Register">
                </div>
            </div>
        </div>
    </form>
</div>