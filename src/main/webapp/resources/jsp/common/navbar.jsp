<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: Roman
  Date: 26.12.2016
  Time: 22:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="i18n.msg"/>
<c:set var="user" value="${currentUser}"/>
<c:set var="hostels" value="${hostels}" scope="request"/>
<c:set var="messages" value="${messages}"/>
<nav class="navbar navbar-expand-lg fixed-top navbar-dark navbar-bg-color">
    <a class="navbar-brand" href="#">
        <i class="fa fa-lg fa-cab" aria-hidden="true"></i>&nbsp Buber
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse ml-3" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">About Us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Contacts</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="language" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Ru&nbsp;<i class="fa fa-globe" aria-hidden="true"></i>
                </a>
                <div class="dropdown-menu" aria-labelledby="language">
                    <a class="dropdown-item" href="#">Russian</a>
                    <a class="dropdown-item" href="#">English</a>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <div class="collapse a">
                <div class="input-group">
                    <div class="input-group-prepend">
                    <span class="input-group-text" id="email">
                        <i class="fa fa-envelope-o fa-fw my-1" aria-hidden="true"></i>
                    </span>
                    </div>
                    <input type="email" class="form-control mr-sm-2" placeholder="Email address"
                           aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>
            </div>
            <div class="collapse a">
                <div class="input-group">
                    <div class="input-group-prepend">
                    <span class="input-group-text" id="password">
                        <i class="fa fa-key fa-fw my-1" aria-hidden="true"></i>
                    </span>
                    </div>
                    <input type="password" class="form-control mr-sm-2" placeholder="Password"
                           aria-label="Default" aria-describedby="inputGroup-sizing-default">
                </div>
            </div>
            <button data-toggle="collapse" data-target=".a" aria-expanded="false"
                    aria-controls="collapseExample" class="btn btn-outline-info my-2 my-sm-0 mx-1" type="button">Sign In
            </button>
            <button type="button" class="btn btn-outline-info my-2 my-sm-0 mx-1" data-toggle="modal"
                    data-target="#exampleModalCenter">
                Register
            </button>
        </form>
    </div>
</nav>
<c:import url="resources/jsp/modal/registermodal.jsp"/>