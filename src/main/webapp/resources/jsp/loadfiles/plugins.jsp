<%--
  Created by IntelliJ IDEA.
  User: Artem
  Date: 11.01.2018
  Time: 15:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<c:url value="/resources/js/lib/jquery.1.10.2.min.js"/>"></script>
<script src="<c:url value="/resources/js/lib/jquery.i18n.properties-min-1.0.9.js"/>"></script>
<script src="<c:url value="/resources/js/lib/jquery-ui.min.js"/>"></script>
<script src="<c:url value="/resources/js/lib/bootstrap.js"/>"></script>
<script src="<c:url value="/resources/js/lib/bootstrap.bundle.js"/>"></script>
<script src="<c:url value="/resources/js/lib/parsley.min.js"/>"></script>

<script src="<c:url value="/resources/js/app/service/MenuBarService.js"/>"></script>
<script src="<c:url value="/resources/js/app/controller/MenuBarController.js"/>"></script>
<script src="<c:url value="/resources/js/app/controller/DateController.js"/>"></script>
<script src="<c:url value="/resources/js/app/pageupdate.js"/>"></script>
<script src="<c:url value="/resources/js/i18n/en.js"/>"></script>
<script src="<c:url value="/resources/js/i18n/ru.js"/>"></script>
<script>
    $( document ).ready(function() {
        window.Parsley.setLocale($("#locale").val());
    });
</script>
<script src="<c:url value="/resources/js/parsley/validator.js"/>"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
