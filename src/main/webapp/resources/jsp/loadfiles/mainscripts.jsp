<%--
  Created by IntelliJ IDEA.
  User: Artem
  Date: 11.01.2018
  Time: 15:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<link href="<c:url value="/resources/css/lib/bootstrap.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/lib/jquery-ui.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/lib/font-awesome.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/navbar.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">


