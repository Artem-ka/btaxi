<%@page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="i18n.msg"/>
<html>
<head>
    <c:import url="resources/jsp/loadfiles/mainscripts.jsp"/>
    <c:import url="resources/jsp/loadfiles/plugins.jsp"/>
    <title><fmt:message key="page.title"/></title>
</head>
<body class="body-style">
<c:import url="resources/jsp/common/navbar.jsp"/>
<div class="jumbotron h-100 my-0 unselectable-text" style="background-color: transparent;">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <a class="carousel-item active" href="##">
                <img class="d-block w-100 h-100" src="resources/taxi.png" alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                    <h1 class="display-2">Available</h1>
                    <p class="lead">hfksdsdsd</p>
                </div>
            </a>
            <div class="carousel-item">
                <img class="d-block w-100 h-100" src="resources/safe.jpg" alt="Second slide">
                <div class="carousel-caption d-none d-md-block">
                    <h1 class="display-2">Safe</h1>
                    <p class="lead">hfksdsdsd</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 h-100" src="resources/fast1.jpg" alt="Second slide">
                <div class="carousel-caption d-none d-md-block">
                    <h1 class="display-2">Fast</h1>
                    <p class="lead">hfksdsdsd</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100 h-100" src="resources/pay2.jpg" alt="Third slide">
                <div class="carousel-caption d-none d-md-block">
                    <h1 class="display-2">Easy To Pay</h1>
                    <p class="lead">hfksdsdsd</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<c:import url="resources/jsp/common/footer.jsp"/>
</body>
</html>