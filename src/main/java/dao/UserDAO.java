package dao;

import connectionpool.ProxyConnection;
import entity.User;
import exception.DAOException;

import java.sql.*;

public class UserDAO extends AbstractDAO<User>{
    private static final String SQL_ADD_USER = "{CALL buber.addNewUser(?, ?, ?, ?, ?, ?, ?)}";

    public UserDAO(){
    }

    public UserDAO(ProxyConnection connection) {
        super(connection);
    }

    @Override
    public boolean create(User entity) throws DAOException {
        CallableStatement cs = null;
        int affectedRows = 0;
        try{
            cs = proxyConnection.prepareCall(SQL_ADD_USER);
            cs.getGeneratedKeys();
            cs.setString(1, entity.getEmail());
            cs.setString(2, entity.getPassword());
            cs.setString(3, entity.getSalt());
            cs.setString(4, entity.getFirstName());
            cs.setString(5, entity.getSecondName());
            cs.setString(6, entity.getLastName());
            cs.setLong(7, entity.getPhone());
            affectedRows = cs.executeUpdate();
            try (ResultSet generatedKeys = cs.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    System.out.println(generatedKeys.getLong(1));
                }
                else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        } catch (SQLException e){
            System.err.println(e.getMessage());
            throw new DAOException(e);
        }

        return affectedRows > -1;
    }
}
