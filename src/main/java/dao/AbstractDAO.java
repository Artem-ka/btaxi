package dao;

import connectionpool.ProxyConnection;
import entity.Entity;
import exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractDAO<T extends Entity> {
    protected static final Logger LOG = LogManager.getLogger();
    ProxyConnection proxyConnection;

    AbstractDAO() {
    }
    AbstractDAO(ProxyConnection proxyConnection) {
        this.proxyConnection = proxyConnection;
    }

    public abstract boolean create(T entity) throws DAOException;

    public void closeStatement(Statement st) {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            LOG.error("Cannot close statement: ", e);
        }
    }

}
