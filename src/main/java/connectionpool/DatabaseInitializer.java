package connectionpool;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class DatabaseInitializer {
    private static final String BUNDLE_NAME = "database";
    private static final String DB_URL_KEY = "db.url";
    private static final String DB_USER_KEY = "db.user";
    private static final String DB_PASSWORD_KEY = "db.password";
    private static final String DB_POOL_SIZE_KEY = "db.poolsize";

    final String  DB_URL;
    final String DB_USER;
    final String DB_PASSWORD;
    final int DB_POOL_SIZE;


    public DatabaseInitializer(){
        try {
            ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME);
            DB_URL = bundle.getString(DB_URL_KEY);
            DB_USER = bundle.getString(DB_USER_KEY);
            DB_PASSWORD = bundle.getString(DB_PASSWORD_KEY);
            DB_POOL_SIZE = Integer.parseInt(bundle.getString(DB_POOL_SIZE_KEY));
        } catch (MissingResourceException e){
            throw new RuntimeException("DB initializer error"); //TODO
        }
    }


}
