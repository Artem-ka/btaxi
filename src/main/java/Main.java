import connectionpool.ConnectionPool;
import dao.UserDAO;
import entity.User;
import exception.DAOException;

import java.sql.*;
import java.util.Properties;
import java.util.ResourceBundle;

public class Main {
    public static void main(String[] args) throws SQLException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        User me = new User(
                "t2323esst@gmail.com",
                "tes23s23t",
                "s2s323",
                "teds3tF32",
                "ted3stS23",
                "tsest3L23",
                1453556623
        );

        UserDAO userDAO = new UserDAO(connectionPool.getConnection());
        try {
            userDAO.create(me);
        }catch (DAOException e){
            System.err.println("MY DAO EXCEPTION");
        }
    }
}
